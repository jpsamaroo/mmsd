Message hierarchy
=================

Service		org.ofono.mms
Interface	org.ofono.mms.ModemManager
Object path	/org/ofono/mms


Methods

    PushNotify()

        Gvariant input Format String    (ay)
            This is the data contents of the SMS WAP

	ChangeSettings()
	    Change a setting in the Modem Manager Plugin
	    IMPORTANT NOTE: Settings changed here will work right away, but any
	                    messages sent to the mmsd-tng queue need to be processed
	                    again. The easiest way to do this is to reset mmsd-tng.
	                    But it can be done with the dbus proxy call
	                    ProcessMessageQueue()


	    Gvariant Input Format String    ((sv))

	ViewSettings()
	    Change all Modem Manager related settings
	    Gvariant Output Format String    (a{sv})

    ProcessMessageQueue()
        This manually activates the Modem Manager Bearer to process any messages
        not sent or received yet. The primary idea behind is two fold:

        a) If the Bearer Handler is misconfigured, the OS/higher level program
           can change the settings via the dbus and test the bearer handler
           to confirm it works.

        b) If modem data is disconnected (manually or due to modem suspend),
           the OS/higher level program can also track this and can command
           mmsd-tng to now process any messages it needs to send/recieve once
           modem data is active.

        Since BearerHandlerError() emits a signal for any errors activating the
        modem contect, ProcessMessageQueue() does not return any value.

Signals
    BearerHandlerError()
        If the bearer handler has an issue activating the context, it will emit
        a signal of the error. The errors are shown above.

        NOTE: MMSD_MM_MODEM_CONTEXT_ACTIVE will never be emitted as a signal.

        Gvariant Output Format String    (h)
            enum {
                  MMSD_MM_MODEM_MMSC_MISCONFIGURED, //the MMSC is the default value
                  MMSD_MM_MODEM_NO_BEARERS_ACTIVE, //The Modem has no bearers
                  MMSD_MM_MODEM_INTERFACE_DISCONNECTED, //mmsd-tng found the right bearer, but it is disconnected
                  MMSD_MM_MODEM_INCORRECT_APN_CONNECTED, //no APN is connected that matches the settings
                  MMSD_MM_MODEM_FUTURE_CASE_DISCONNECTED, //Reserved for future case
                  MMSD_MM_MODEM_CONTEXT_ACTIVE //No error, context activated properly
            } mm_context_connection;

    SettingsChanged()
        mmsd-tng changed the MMS settings based on an auto configureation

        Gvariant Output Format String    (sss)
        	apn: The MMS APN (may be identical to the internet APN based on carrier settings)
        	mmsc: The MMSC
        	proxy: The MMS Proxy (if there is none, it will be sent as "NULL")
Properties	None
